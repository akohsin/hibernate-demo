package pl.sda.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import pl.sda.hibernate.domain.Employee;
import pl.sda.hibernate.util.HibernateUtil;

public class HibernateDemo {

    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();
            Employee employee = session.get(Employee.class, 23);
            System.out.println("employee = " + employee);

            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
}
